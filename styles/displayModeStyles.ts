import { StyleSheet } from 'react-native';

const displayModeStyles = StyleSheet.create({
    darkMode: {
        color: 'white',
        backgroundColor: 'black',
    },
    lightMode: {
        color: 'black',
        backgroundColor: 'light',
    },
});

export default displayModeStyles;
