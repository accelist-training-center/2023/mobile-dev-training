import { NavigationContainer } from '@react-navigation/native';
import { RootNavigationStack } from './components/navigations/Root/RootNavigationStack';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { StatusBar } from 'expo-status-bar';
import { QueryClient, QueryClientProvider, focusManager, onlineManager } from '@tanstack/react-query';
import NetInfo from '@react-native-community/netinfo';
import { AppState, AppStateStatus, Platform, View } from 'react-native';
import React, { useEffect } from 'react';
import displayModeStyles from './styles/displayModeStyles';

// Create a TanStack QueryClient object.
const queryClient = new QueryClient();

export default function App() {
  // TanStack Query online status registration.
  onlineManager.setEventListener(setOnline => {
    return NetInfo.addEventListener(state => {
      setOnline(!!state.isConnected);
    });
  });

  // TanStack Query refetch on App focus registration.
  function onAppStateChange(status: AppStateStatus) {
    if (Platform.OS !== 'web') {
      focusManager.setFocused(status === 'active');
    }
  }

  // TanStack Query refetch on App focus registration.
  useEffect(() => {
    const subscription = AppState.addEventListener('change', onAppStateChange);

    return () => subscription.remove();
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <SafeAreaProvider style={displayModeStyles.darkMode}>
        <NavigationContainer>
          <RootNavigationStack />
          <StatusBar style="auto" />
        </NavigationContainer>
      </SafeAreaProvider>
    </QueryClientProvider>
  );
}
