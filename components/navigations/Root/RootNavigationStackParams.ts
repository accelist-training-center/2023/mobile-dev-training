/**
 * Type definitions for Root Navigation Stack navigator.
 */
export type RootNavigationStackParams = {
    // If your route doesn't contains any params, declare it as undefined.
    RootBottomTab: undefined,
}
