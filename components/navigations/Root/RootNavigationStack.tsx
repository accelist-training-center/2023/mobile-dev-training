import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RootBottomTab } from '../RootBottomTab/RootBottomTab';
import { RootNavigationStackParams } from './RootNavigationStackParams';

const Stack = createNativeStackNavigator<RootNavigationStackParams>();

export const RootNavigationStack: React.FC = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="RootBottomTab" component={RootBottomTab} options={{
                headerShown: false,
            }} />
        </Stack.Navigator>
    );
};