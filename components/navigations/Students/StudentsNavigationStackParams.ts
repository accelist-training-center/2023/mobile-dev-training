/**
 * Type definitions for Students Navigation Stack navigator.
 */
export type StudentsNavigationStackParams = {
    StudentsMenuScreen: undefined,
    StudentListScreen: undefined,
    SchoolListScreen: undefined,
    AddNewStudentScreen: undefined,
}
