import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StudentsMenuScreen } from './screens/StudentsMenuScreen';
import { SchoolListScreen } from './screens/SchoolListScreen';
import { StudentListScreen } from './screens/StudentListScreen';
import { StudentsNavigationStackParams } from './StudentsNavigationStackParams';
import { AddStudentScreen } from './screens/AddStudentScreen';

const Stack = createNativeStackNavigator<StudentsNavigationStackParams>();

export const StudentsNavigationStack: React.FC = () => {
    return (
        <Stack.Navigator>
            {/*
            The first registered Stack.Screen component in here will be displayed
            when this Stack Navigator component got rendered.
            */}
            <Stack.Screen name="StudentsMenuScreen" component={StudentsMenuScreen}
                options={{
                    title: 'Student Menu',
                }} />
            <Stack.Screen name="StudentListScreen" component={StudentListScreen} />
            <Stack.Screen name="SchoolListScreen" component={SchoolListScreen} />
            <Stack.Screen name="AddNewStudentScreen" component={AddStudentScreen} options={{
                title: 'Add New Student',
            }}/>
        </Stack.Navigator>
    );
};
