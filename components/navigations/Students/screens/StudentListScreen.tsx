import { View, Text, FlatList, StyleSheet, Button } from 'react-native';
import { StudentsNavigationStackScreenProps } from '../../StackScreenProps';

export const StudentListScreen: React.FC<StudentsNavigationStackScreenProps<'StudentListScreen'>> = (props) => {
    const students: string[] = [];

    return (
        <View style={styles.container}>
            <Text>Student List:</Text>
            <FlatList data={students}
                renderItem={(student) => {
                    return <View key={student.index} style={styles.row}>
                        <Text style={styles.text}>{student.item}</Text>
                    </View>;
                }}
            />
            <Button title="Add"
                onPress={() => props.navigation.navigate('AddNewStudentScreen')}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    row: {
        flex: 1,
        borderColor: 'grey',
        borderWidth: 1,
        paddingVertical: 20,
        paddingHorizontal: 10,
    },
    text: {
        fontSize: 20,
    },
});
