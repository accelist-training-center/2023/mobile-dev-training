import { useState } from 'react';
import { TextInput, Text, View, StyleSheet, ScrollView, Button } from 'react-native';
import { SubmissionMessage } from '../../../../interfaces/SubmissionMessage';
import { Controller, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';
import axios from 'axios';
import { studentApiUri } from '../../../../constants/apiUri';

// Define the add student form schema.
const AddStudentFormSchema = z.object({
    name: z.string()
        .nonempty('Name must be filled').min(3, {
            message: 'Name length must be higher than 2.',
        }),
    weight: z.number().min(31, {
        message: 'Weight must be higher than 30.',
    })
        .max(120, {
            message: 'Weight must be lower than 120.',
        }),
    address: z.string().nonempty('Address must be filled').max(500, {
        message: 'Address length must be lower than 500.',
    }),
});

// Extract the schema into its own TypeScript type definitions.
type AddStudentFormModel = z.infer<typeof AddStudentFormSchema>;

export const AddStudentScreen: React.FC = () => {
    const [message, setMessage] = useState<SubmissionMessage>({
        isSuccess: false,
        message: '',
    });

    /**
     * When submitting, you should prevent the users from rapidly tapping the submit button.
     * You can use isSubmitting flag.
     */
    const { control, handleSubmit, formState: { errors, isSubmitting } } = useForm<AddStudentFormModel>({
        resolver: zodResolver(AddStudentFormSchema),
        defaultValues: {
            name: '',
            weight: 0,
            address: '',
        },
    });

    function renderSubmissionMessage() {
        if (!message.message) {
            return;
        }

        let textColor = 'red';

        if (message.isSuccess) {
            textColor = 'green';
        }

        return <Text style={[styles.text, { color: textColor }]}>
            {message.message}
        </Text>;
    }

    async function onSubmit(newStudent: AddStudentFormModel) {

        try {
            await axios.post(studentApiUri, newStudent);

            setMessage({
                isSuccess: true,
                message: 'Successfully added a new student.',
            });
        } catch (error) {
            setMessage({
                isSuccess: false,
                message: `Error when connecting to server. ${error}`,
            });
        }
    }

    /**
     * Convert input string value into number.
     * If NaN, this function will return 0 value.
     * @param value
     * @returns
     */
    function convertInputStringToNumber(value: string) {
        const numberValue = parseInt(value, 10);

        if (isNaN(numberValue)) {
            return 0;
        }

        return numberValue;
    }

    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.text}>Name:</Text>
                <Controller control={control}
                    name="name"
                    render={({ field: { value, onBlur, onChange } }) =>
                        <TextInput style={[styles.text, styles.input]}
                            value={value}
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                    } />
                {errors.name && <Text style={styles.error}>{errors.name.message}</Text>}

                <Text style={styles.text}>Weight:</Text>
                <Controller control={control}
                    name="weight"
                    render={({ field: { value, onBlur, onChange } }) =>
                        <TextInput style={[styles.text, styles.input]}
                            value={value?.toString()}
                            onBlur={onBlur}
                            onChangeText={(weightValue) => onChange(convertInputStringToNumber(weightValue))}
                            inputMode="numeric"
                            keyboardType="numeric" />
                    } />
                {errors.weight && <Text style={styles.error}>{errors.weight?.message}</Text>}

                <Text style={styles.text}>Address:</Text>
                <Controller control={control}
                    name="address"
                    render={({ field: { value, onBlur, onChange } }) =>
                        <TextInput style={[styles.text, styles.input]} value={value.toString()}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            multiline numberOfLines={4} />
                    } />
                {errors.address && <Text style={styles.error}>{errors.address?.message}</Text>}

                <Button title="Submit" disabled={isSubmitting} onPress={handleSubmit(onSubmit)} />

                {renderSubmissionMessage()}
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    text: {
        fontSize: 18,
        marginTop: 12,
        marginBottom: 2,
    },
    input: {
        borderWidth: 1,
        padding: 10,
        backgroundColor: 'white',
    },
    error: {
        fontSize: 18,
        color: 'red',
        marginBottom: 6,
    },
});
