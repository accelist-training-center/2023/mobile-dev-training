import { Pressable, ScrollView, View, StyleSheet, Text } from 'react-native';
import { StudentsNavigationStackScreenProps } from '../../StackScreenProps';

export const StudentsMenuScreen: React.FC<StudentsNavigationStackScreenProps<'StudentsMenuScreen'>> =
    ({ navigation }) => {
        function navigateTo(
            screen: 'StudentsMenuScreen'
                | 'StudentListScreen'
                | 'SchoolListScreen'
        ) {
            navigation.navigate(screen);
        }

        return (
            <ScrollView>
                <View>
                    <Pressable onPressOut={() => navigateTo('StudentListScreen')}>
                        <View style={styles.row}>
                            <Text style={styles.text}>Student List</Text>
                        </View>
                    </Pressable>
                    <Pressable onPressOut={() => navigateTo('SchoolListScreen')}>
                        <View style={styles.row}>
                            <Text style={styles.text}>School List</Text>
                        </View>
                    </Pressable>
                </View>
            </ScrollView>
        );
    };

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    row: {
        flex: 1,
        borderColor: 'grey',
        borderWidth: 1,
        paddingVertical: 20,
        paddingHorizontal: 10,
    },
    text: {
        fontSize: 20,
    },
});
