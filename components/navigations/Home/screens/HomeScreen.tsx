import { FlatList, Pressable, StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import NewsListModel from '../../../../interfaces/NewsListModel';
import { HomeRootBottomTabCompositeScreenProps } from '../../CompositeNavigationProps';
import { useQuery } from '@tanstack/react-query';
import { newsApiUri } from '../../../../constants/apiUri';
import { defaultFetcher } from '../../../../functions/createAxiosFetcher';
import { useRefreshOnFocus } from '../../../../functions/useRefreshOnFocus';
import { useAtom } from 'jotai';
import { displayModeAtom } from '../../../../atoms/global';

export const HomeScreen: React.FC<HomeRootBottomTabCompositeScreenProps<'HomeScreen'>> = (props) => {
    const [displayMode] = useAtom(displayModeAtom);
    const insets = useSafeAreaInsets();
    const insetsStyle = StyleSheet.create({
        insets: {
            paddingTop: insets.top,
            paddingBottom: insets.bottom,
            paddingLeft: insets.left,
            paddingRight: insets.right,
        },
    });

    const { data, refetch } = useQuery<NewsListModel[]>({
        queryKey: [],
        queryFn: async () => await defaultFetcher(newsApiUri),
    });
    // Refresh on this screen focus.
    useRefreshOnFocus(refetch);

    // function renderRows() {
    //     const rowDataComponent = data.map((value, index) =>
    //         <View key={index} style={styles.row}>
    //             <Text style={styles.text}>{value}</Text>
    //         </View>
    //     );

    //     return rowDataComponent;
    // }

    function navigateToDetail(news: NewsListModel) {
        props.navigation.navigate('HomeDetailScreen', {
            id: news.id,
            title: news.title,
        });
    }

    return (
        // ScrollView example.
        // <ScrollView>
        //     <View style={[styles.container, {
        //         paddingTop: insets.top,
        //         paddingBottom: insets.bottom,
        //         paddingLeft: insets.left,
        //         paddingRight: insets.right
        //     }]}>
        //         {renderRows()}
        //     </View>
        // </ScrollView>
        <View>
            <FlatList data={data}
                renderItem={(news) =>
                    <Pressable onPressOut={() => navigateToDetail(news.item)}>
                        <View key={news.index} style={styles.row}>
                            <Text style={styles.text}>{news.item.title}</Text>
                        </View>
                    </Pressable>
                }
                style={insetsStyle.insets}
            />
            <Text>You are using {displayMode} mode</Text>
        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    row: {
        flex: 1,
        borderColor: 'grey',
        borderWidth: 1,
        paddingVertical: 20,
        paddingHorizontal: 10,
    },
    text: {
        fontSize: 20,
    },
});
