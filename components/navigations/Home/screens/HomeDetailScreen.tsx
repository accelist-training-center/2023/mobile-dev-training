import { View, Text, StyleSheet, Image, useWindowDimensions, ScrollView } from 'react-native';
import { HomeRootBottomTabCompositeScreenProps } from '../../CompositeNavigationProps';
import { useEffect, useState } from 'react';

export const HomeDetailScreen: React.FC<HomeRootBottomTabCompositeScreenProps<'HomeDetailScreen'>> = (props) => {
    const [description, setDescription] = useState<string>();
    const { height, width } = useWindowDimensions();

    // When rendering the description text.
    useEffect(() => {
        let descriptionText = 'Survey training 3 + 1 2023 sudah dimulai dan para peserta wajib hadir selama mengikuti kegiatan training sampai selesai.';

        if (props.route.params.id === 1) {
            descriptionText = 'Kurikulum mobile app development dengan React Native dan Expo telah diselenggarakan dan para peserta wajib mengikuti kegiatan training.';
        }
        setDescription(descriptionText);
    }, [props.route.params.id]);

    // When rendering the header text.
    useEffect(() => {
        props.navigation.setOptions({ title: props.route.params.title });
    }, [props.navigation, props.route.params.title]);

    return (
        <ScrollView>
            <View style={styles.container}>
                {/*
                    Example of static image rendering.
                */}
                {/* <Image style={{
                    width: width,
                    height: height - (height * 0.75),
                }} source={require('../../../../assets/samples/sample-bg.png')} /> */}

                <Image style={{
                    width: width,
                    height: height - (height * 0.75),
                }} source={{
                    uri: 'https://i.ytimg.com/vi/fXmHKu0WMis/maxresdefault.jpg',
                }} />
                <Text>{description}</Text>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    image: {
        width: '90%',
    },
})