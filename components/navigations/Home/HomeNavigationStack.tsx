import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { HomeNavigationStackParams } from './HomeNavigationStackParams';
import { HomeScreen } from './screens/HomeScreen';
import { HomeDetailScreen } from './screens/HomeDetailScreen';

const Stack = createNativeStackNavigator<HomeNavigationStackParams>();

export const HomeNavigationStack: React.FC = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="HomeScreen" component={HomeScreen}
            options={{
                headerShown: false,
            }} />
            <Stack.Screen name="HomeDetailScreen" component={HomeDetailScreen}
            options={{
            }} />
        </Stack.Navigator>
    );
};
