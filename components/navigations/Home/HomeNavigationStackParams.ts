/**
 * Type definitions for Home Stack navigator.
 */
export type HomeNavigationStackParams = {
    HomeScreen: undefined,
    HomeDetailScreen: {
        id: number,
        title: string,
    },
}
