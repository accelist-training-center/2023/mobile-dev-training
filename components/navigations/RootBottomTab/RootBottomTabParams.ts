/**
 * Type definitions for Root Bottom Tab navigator.
 */
export type RootBottomTabParams = {
    Home: undefined,
    Students: undefined,
    Trainers: undefined,
    Settings: undefined,
}
