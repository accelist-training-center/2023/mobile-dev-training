import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import FontAwesome from '@expo/vector-icons/FontAwesome';
import FontAwesome5 from '@expo/vector-icons/FontAwesome5';
import { StudentsNavigationStack } from '../Students/StudentsNavigationStack';
import { RootBottomTabParams } from './RootBottomTabParams';
import { HomeNavigationStack } from '../Home/HomeNavigationStack';
import { SettingsScreen } from './screens/SettingsScreen';

const Tab = createBottomTabNavigator<RootBottomTabParams>();

export const RootBottomTab: React.FC = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Home" component={HomeNavigationStack}
            options={{
                headerShown: false,
                tabBarIcon: ({color, size}) =>
                    // To render the icon, use the imported FontAwesome component
                    // and set the name props with the proper icon name.
                    <FontAwesome name="home" color={color} size={size} />
            }} />
            <Tab.Screen name="Students" component={StudentsNavigationStack}
            options={{
                headerShown: false,
                tabBarIcon: ({color, size}) => 
                    <FontAwesome name="graduation-cap" color={color} size={size} />
            }} />
            <Tab.Screen name="Trainers" component={StudentsNavigationStack} options={{
                headerShown: false,
                tabBarIcon: ({color, size}) =>
                    <FontAwesome5 name="chalkboard-teacher" color={color} size={size} />
            }} />
            <Tab.Screen name="Settings" component={SettingsScreen} options={{
                tabBarIcon: ({color, size}) =>
                <FontAwesome name="gear" color={color} size={size} />
            }}/>
        </Tab.Navigator>
    );
};
