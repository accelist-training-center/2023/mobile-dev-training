import { useAtom } from 'jotai';
import { Pressable, StyleSheet, Text, View } from 'react-native';
import { displayModeAtom } from '../../../../atoms/global';

export const SettingsScreen: React.FC = () => {
    const [displayMode, setDisplayMode] = useAtom(displayModeAtom);

    function toggleDisplayMode(mode: 'light' | 'dark') {
        setDisplayMode(mode);
    }

    return (
        <View style={styles.container}>
            <Text style={styles.optionHeaderText}>Display Mode:</Text>
            <View style={styles.optionContainer}>
                <Pressable style={styles.optionPressable} onPress={() => toggleDisplayMode('light')}>
                    <Text style={styles.optionText}>{'Light '}
                    {
                        displayMode === 'light' &&
                        <Text>(Selected)</Text>
                    }
                    </Text>
                </Pressable>
                <Pressable style={styles.optionPressable} onPress={() => toggleDisplayMode('dark')}>
                    <Text style={styles.optionText}>{'Dark '}
                    {
                        displayMode === 'dark' &&
                        <Text>(Selected)</Text>
                    }</Text>
                </Pressable>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
    },
    optionHeaderText: {
        fontSize: 24,
        marginVertical: 10,
    },
    optionContainer: {
        // flex: 1,
        // justifyContent: 'flex-start',
        borderColor: 'grey',
        borderWidth: 1,
    },
    optionPressable: {
        borderColor: 'grey',
        borderWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    optionText: {
        color: 'black',
        fontSize: 20,
    },
});
