import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { CompositeScreenProps } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { HomeNavigationStackParams } from './Home/HomeNavigationStackParams';
import { RootBottomTabParams } from './RootBottomTab/RootBottomTabParams';

/**
 * Register the composite navigation / screen navigators / screens when
 * you need to access those props in a screen component.
 */
export type HomeRootBottomTabCompositeScreenProps<T extends keyof HomeNavigationStackParams> =
    CompositeScreenProps<
        NativeStackScreenProps<HomeNavigationStackParams, T>,
        BottomTabScreenProps<RootBottomTabParams>
    >;
