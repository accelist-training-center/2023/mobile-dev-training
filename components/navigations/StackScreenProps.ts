import { StudentsNavigationStackParams } from './Students/StudentsNavigationStackParams';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RootNavigationStackParams } from './Root/RootNavigationStackParams';
import { HomeNavigationStackParams } from './Home/HomeNavigationStackParams';

/**
 * This type definition is easier to call, for example, if you need to specify the props type in the HomeScreen,
 * you could just define it as with RootNavigationStackScreenProps<'RootBottomTab'>.
 */
export type RootNavigationStackScreenProps<T extends keyof RootNavigationStackParams> =
    // Use NativeStackScreenProps for Native Stack components.
    NativeStackScreenProps<RootNavigationStackParams, T>;

export type StudentsNavigationStackScreenProps<T extends keyof StudentsNavigationStackParams> =
    NativeStackScreenProps<StudentsNavigationStackParams, T>;

export type HomeNavigationStackScreenProps<T extends keyof HomeNavigationStackParams> =
    NativeStackScreenProps<HomeNavigationStackParams, T>;
