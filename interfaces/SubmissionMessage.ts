/**
 * Model for binding the submission message object for displaying the form submission status.
 */
export interface SubmissionMessage {
    isSuccess: boolean,
    message: string,
}
