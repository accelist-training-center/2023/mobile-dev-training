/**
 * Model definitions for binding the news list data in the Home screen.
 */
export default interface NewsListModel {
    id: number,
    title: string,
    timestamp: string,
}
