// Change localhost with WLAN IP address of your computer that is hosting the BE app.
export const serverUri = 'http://localhost:5096';

export const newsApiUri = serverUri + '/api/v1/home';

export const studentApiUri = serverUri + '/api/v1/student';
