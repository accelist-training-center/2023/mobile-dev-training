import axios from 'axios';

export function createAxiosFetcher() {
    return async (url: string) => {
        const response = await axios.get(url);


        return response.data;
    };
}

export const defaultFetcher = createAxiosFetcher();
