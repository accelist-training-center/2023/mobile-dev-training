import { atom } from 'jotai';

export const displayModeAtom = atom<'light' | 'dark'>('light');
